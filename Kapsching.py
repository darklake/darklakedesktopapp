import re
import sys
from Firebase import Firebase
from GUI import Ui_MainWindow 
from PyQt5.QtWidgets import QMessageBox
from PyQt5 import QtCore, QtGui, QtWidgets


class Kapsching(Ui_MainWindow):
    def __init__(self):
        app = QtWidgets.QApplication(sys.argv)
        mainWindow = QtWidgets.QMainWindow()
        mainWindow.setWindowIcon(QtGui.QIcon("Resources/logo.png"))

        self._firebase = Firebase(self.getKeyPath())
        self._shouldUpdate = False
        self._shouldDelete = False

        self.setupUi(mainWindow)
        mainWindow.show()
        sys.exit(app.exec_())

    @staticmethod
    def getKeyPath():
        ''' Open a dialog were the usere can locate the admin key '''
        filePath = str()
        try:
            with open('Resources/keyPath', 'r') as f:
                filePath = f.readline()
        except:
            dialog = QtWidgets.QFileDialog(None, "Locate firebase key", "./", "Json (*.json)")
            if dialog.exec():
                with open('Resources/keyPath', 'w') as f:
                    f.write(dialog.selectedFiles()[0])
                filePath = dialog.selectedFiles()[0]
            else:
                quit()
        return filePath

    def setupUi(self, MainWindow):
        ''' Sets up the GUI '''
        Ui_MainWindow.setupUi(self, MainWindow)
        self.backBtn.clicked.connect(self.returnToStart)
        self.searchBtn.clicked.connect(self.searchGantry)
        self.publishGantryBtn.clicked.connect(self.addNewGantry)
        self.updateGantryBtn.clicked.connect(self.updateGantryBtn_clicked)
        self.deleteGantryBtn.clicked.connect(self.deleteGantryBtn_clicked)
        self.registerGantryBtn.clicked.connect(self.registerGantryBtn_clicked)
        self.backBtn.setIcon(QtGui.QIcon("Resources/backArrow.png"))
        self.kapschLogo1.setPixmap(QtGui.QPixmap("Resources/KapschingLogo.png"))
        self.kapschLogo2.setPixmap(QtGui.QPixmap("Resources/KapschingLogo.png"))
        self.gantryCost.setValidator(QtGui.QDoubleValidator(0, 1000, 2))
        self._messageBox = QMessageBox()
        self._messageBox.setIconPixmap(QtGui.QPixmap("Resources/logo.png"))

    def registerGantryBtn_clicked(self):
        ''' Do what is needed when the "registerGantryBtn" is clicked '''
        self.searchBtn.hide()
        self.stackedWidget.setCurrentIndex(1)
        self.publishGantryBtn.setText('Publish Gantry')

    def updateGantryBtn_clicked(self):
        ''' Do what is needed when the "updateGantryBtn" is clicked '''
        self.searchBtn.show()
        self.gantryName.hide()
        self.gantryCost.hide()
        self._shouldUpdate = True
        self.gantryCostLabel.hide()
        self.gantryNameLabel.hide()
        self.publishGantryBtn.hide()
        self.stackedWidget.setCurrentIndex(1)
        self.publishGantryBtn.setText('Update Gantry')

    def deleteGantryBtn_clicked(self):
        ''' Do what is needed when the "deleteGantryBtn" is clicked '''
        self.gantryName.hide()
        self.gantryCost.hide()
        self._shouldDelete = True
        self.gantryCostLabel.hide()
        self.gantryNameLabel.hide()
        self.publishGantryBtn.hide()
        self.stackedWidget.setCurrentIndex(1)
        self.publishGantryBtn.setText('Delete Gantry')

    def okPopUpMessage(self, title, info):
        ''' Displays a popup message with an ok button '''
        return self._messageBox.question(None, title, info, QMessageBox.Ok, QMessageBox.Ok)
    
    def yesNoPopUpMessage(self, title, info):
        ''' Displays a popup message with an yes and no button '''
        return self._messageBox.question(None, title, info, QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

    def getValidId(self, gantryID):
        ''' Checks if the "gantryID" is valid if so, returns the valid id '''
        try:
            gantryId = re.findall(r'([\w\d]{2}:[\w\d]{2}:[\w\d]{2}:[\w\d]{2}:[\w\d]{2}:[\w\d]{2})$', gantryID)
            if len(gantryId) > 0:
                return gantryId[0]
            else:
                self.okPopUpMessage('Invalid gantry ID', '"{}" is not a valid gantry ID.\nPlease try agian.'.format(self.gantryId.text()))
        except:
            self.okPopUpMessage('Something went wrong', 'Something unexpected went wrong!\nPlease try agian.')

    def searchGantry(self):
        ''' Holds the functionality for when the search button is pressed '''
        try:
            gantryId = self.getValidId(self.gantryId.text())
            if gantryId is not None:
                gantryInfo = self._firebase.readGantryInfo(gantryId)
                if gantryInfo is not None:
                    if self._shouldUpdate:
                        self.gantryName.show()
                        self.gantryCost.show()
                        self.gantryCostLabel.show()
                        self.gantryNameLabel.show()
                        self.publishGantryBtn.show()
                        self.gantryName.setText(gantryInfo['name'])
                        self.gantryCost.setText(str(gantryInfo['cost']))
                    elif self._shouldDelete:
                        self.gantryName.hide()
                        self.gantryCost.hide()
                        self.gantryName.setText('')
                        self.gantryCost.setText('')
                        self.publishGantryBtn.show()
                elif self._shouldUpdate:
                    btnReply = self.yesNoPopUpMessage('Gantry dose not exists', 'Gantry with ID "{}" dose not exist.\nDo you want creat it?'.format(self.gantryId.text()))
                    if btnReply == QMessageBox.Yes:
                        self.returnToStart()
                        self.registerGantryBtn_clicked()
                elif self._shouldDelete:
                    self.okPopUpMessage('Gantry dose not exists', 'Gantry with ID "{}" dose not exist.\nPlease try agian.'.format(self.gantryId.text()))
        except:
            self.okPopUpMessage('Something went wrong', 'Something unexpected went wrong!\nPlease try agian.')

    def addNewGantry(self):
        ''' Holds the functionality for when the button for register a new gantry is pressed '''
        try:
            if (len(self.gantryCost.text()) == 0 or len(self.gantryName.text()) == 0) and not self._shouldDelete:
                self.okPopUpMessage('Empty field', 'You have not filled in all required fields!\nPleas try again.')
            elif self._shouldUpdate:
                gantryId = self.getValidId(self.gantryId.text())
                if gantryId is not None:
                    self._firebase.updateGantryWithoutPosition(gantryId, float(self.gantryCost.text().replace(',', '.')), self.gantryName.text())
                    self.returnToStart()

            elif self._shouldDelete:
                gantryId = self.getValidId(self.gantryId.text())
                if gantryId is not None:
                    btnReply = self.yesNoPopUpMessage('Double check', 'Are you sure that you want to delete the gantry with id: "{}"?'.format(gantryId))
                    if btnReply == QMessageBox.Yes:
                        self._firebase.deleteGantry(gantryId)
                        self.returnToStart()

            else:
                gantryId = self.getValidId(self.gantryId.text())
                if gantryId is not None:
                    gantryInfo = self._firebase.readGantryInfo(gantryId)
                    if gantryInfo is None:
                            bluetoothId = gantryId
                            name = self.gantryName.text()
                            try:
                                cost = float(self.gantryCost.text().replace(',', '.'))
                            except:
                                self.okPopUpMessage('Invalid cost', 'The cost you entered in not valid.\nPleas try again.')
                                return
                            self._firebase.addGantry(bluetoothId, cost, name, (0, 0))
                            self.returnToStart()
                    else:
                        btnReply = self.yesNoPopUpMessage('Gantry already exists', '"{}" already exist.\nDo you want update it?'.format(self.gantryId.text()))
                        if btnReply == QMessageBox.Yes:
                            self.returnToStart()
                            self.updateGantryBtn_clicked()
                            self.gantryName.show()
                            self.gantryCost.show()
                            self.searchGantry()
        except:
            self.okPopUpMessage('Something went wrong', 'Something unexpected went wrong!\nPlease try agian.')                        

    def returnToStart(self):
        ''' Resets the GUI '''
        self.searchBtn.show()
        self.gantryName.show()
        self.gantryCost.show()
        self.gantryName.show()
        self.gantryCost.show()
        self._shouldUpdate = False
        self._shouldDelete = False
        self.gantryCostLabel.show()
        self.gantryNameLabel.show()
        self.gantryName.setText('')
        self.gantryCost.setText('')
        self.publishGantryBtn.show()
        self.stackedWidget.setCurrentIndex(0)


if __name__ == '__main__':
    kapschin = Kapsching()