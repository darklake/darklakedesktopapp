# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1211, 700)
        """ icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../Resources/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon) """
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("background-color: rgb(85, 87, 88);")
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.centralWidget)
        self.horizontalLayout_7.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout_7.setSpacing(6)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.stackedWidget = QtWidgets.QStackedWidget(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.stackedWidget.sizePolicy().hasHeightForWidth())
        self.stackedWidget.setSizePolicy(sizePolicy)
        self.stackedWidget.setObjectName("stackedWidget")
        self.startPage = QtWidgets.QWidget()
        self.startPage.setObjectName("startPage")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.startPage)
        self.verticalLayout_8.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_8.setSpacing(6)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setSpacing(6)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout_7.addItem(spacerItem)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.kapschLogo1 = QtWidgets.QLabel(self.startPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.kapschLogo1.sizePolicy().hasHeightForWidth())
        self.kapschLogo1.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setStyleStrategy(QtGui.QFont.PreferDefault)
        self.kapschLogo1.setFont(font)
        self.kapschLogo1.setText("")
        self.kapschLogo1.setPixmap(QtGui.QPixmap("../Resources/KapschingLogo.png"))
        self.kapschLogo1.setScaledContents(True)
        self.kapschLogo1.setAlignment(QtCore.Qt.AlignCenter)
        self.kapschLogo1.setWordWrap(False)
        self.kapschLogo1.setObjectName("kapschLogo1")
        self.verticalLayout_2.addWidget(self.kapschLogo1)
        spacerItem2 = QtWidgets.QSpacerItem(20, 131, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.MinimumExpanding)
        self.verticalLayout_2.addItem(spacerItem2)
        self.registerGantryBtn = QtWidgets.QPushButton(self.startPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.registerGantryBtn.sizePolicy().hasHeightForWidth())
        self.registerGantryBtn.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.registerGantryBtn.setFont(font)
        self.registerGantryBtn.setStyleSheet("QPushButton {\n"
"  background-color: rgb(0, 0, 0);\n"
"  color: rgb(255, 193, 0);\n"
"  border: 1px;\n"
"  border-radius: 20px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 5,\n"
"                                      stop: 0 #BEBEBE, stop: 1 #323232);\n"
"}\n"
"\n"
"QPushButton:hover:!pressed{\n"
"     background-color: rgb(255, 193, 0);\n"
"     color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.registerGantryBtn.setObjectName("registerGantryBtn")
        self.verticalLayout_2.addWidget(self.registerGantryBtn)
        spacerItem3 = QtWidgets.QSpacerItem(20, 13, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout_2.addItem(spacerItem3)
        self.updateGantryBtn = QtWidgets.QPushButton(self.startPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.updateGantryBtn.sizePolicy().hasHeightForWidth())
        self.updateGantryBtn.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.updateGantryBtn.setFont(font)
        self.updateGantryBtn.setStyleSheet("QPushButton {\n"
"  background-color: rgb(0, 0, 0);\n"
"  color: rgb(255, 193, 0);\n"
"  border: 1px;\n"
"  border-radius: 20px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 5,\n"
"                                      stop: 0 #BEBEBE, stop: 1 #323232);\n"
"}\n"
"\n"
"QPushButton:hover:!pressed{\n"
"     background-color: rgb(255, 193, 0);\n"
"     color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.updateGantryBtn.setObjectName("updateGantryBtn")
        self.verticalLayout_2.addWidget(self.updateGantryBtn)
        spacerItem4 = QtWidgets.QSpacerItem(20, 13, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout_2.addItem(spacerItem4)
        self.deleteGantryBtn = QtWidgets.QPushButton(self.startPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.deleteGantryBtn.sizePolicy().hasHeightForWidth())
        self.deleteGantryBtn.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.deleteGantryBtn.setFont(font)
        self.deleteGantryBtn.setStyleSheet("QPushButton {\n"
"  background-color: rgb(0, 0, 0);\n"
"  color: rgb(255, 193, 0);\n"
"  border: 1px;\n"
"  border-radius: 20px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 5,\n"
"                                      stop: 0 #BEBEBE, stop: 1 #323232);\n"
"}\n"
"\n"
"QPushButton:hover:!pressed{\n"
"     background-color: rgb(255, 193, 0);\n"
"     color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.deleteGantryBtn.setObjectName("deleteGantryBtn")
        self.verticalLayout_2.addWidget(self.deleteGantryBtn)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem5)
        self.verticalLayout_7.addLayout(self.horizontalLayout)
        spacerItem6 = QtWidgets.QSpacerItem(20, 131, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout_7.addItem(spacerItem6)
        self.verticalLayout_8.addLayout(self.verticalLayout_7)
        self.stackedWidget.addWidget(self.startPage)
        self.editPage = QtWidgets.QWidget()
        self.editPage.setObjectName("editPage")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.editPage)
        self.verticalLayout_4.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_4.setSpacing(6)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setSpacing(6)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.backBtn = QtWidgets.QPushButton(self.editPage)
        self.backBtn.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.backBtn.sizePolicy().hasHeightForWidth())
        self.backBtn.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.backBtn.setFont(font)
        self.backBtn.setStyleSheet("QPushButton {\n"
"background-color: rgb(85, 87, 88);\n"
"  color: rgb(255, 193, 0);\n"
"  border: 1px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 5,\n"
"                                      stop: 0 #BEBEBE, stop: 1 #323232);\n"
"}\n"
"\n"
"QPushButton:hover:!pressed{\n"
"     background-color: rgb(0, 0, 0);\n"
"     color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.backBtn.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../Resources/backArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.backBtn.setIcon(icon1)
        self.backBtn.setIconSize(QtCore.QSize(50, 50))
        self.backBtn.setAutoRepeat(False)
        self.backBtn.setObjectName("backBtn")
        self.verticalLayout.addWidget(self.backBtn)
        spacerItem7 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem7)
        self.horizontalLayout_6.addLayout(self.verticalLayout)
        spacerItem8 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem8)
        self.kapschLogo2 = QtWidgets.QLabel(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.kapschLogo2.sizePolicy().hasHeightForWidth())
        self.kapschLogo2.setSizePolicy(sizePolicy)
        self.kapschLogo2.setText("")
        self.kapschLogo2.setPixmap(QtGui.QPixmap("../Resources/KapschingLogo.png"))
        self.kapschLogo2.setScaledContents(True)
        self.kapschLogo2.setObjectName("kapschLogo2")
        self.horizontalLayout_6.addWidget(self.kapschLogo2)
        spacerItem9 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem9)
        self.verticalLayout_3.addLayout(self.horizontalLayout_6)
        spacerItem10 = QtWidgets.QSpacerItem(20, 59, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem10)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.gantryIdLabel = QtWidgets.QLabel(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gantryIdLabel.sizePolicy().hasHeightForWidth())
        self.gantryIdLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.gantryIdLabel.setFont(font)
        self.gantryIdLabel.setStyleSheet("color: rgb(255, 193, 0);\n"
"background-color: rgb(0, 0, 0);")
        self.gantryIdLabel.setScaledContents(True)
        self.gantryIdLabel.setWordWrap(False)
        self.gantryIdLabel.setObjectName("gantryIdLabel")
        self.horizontalLayout_4.addWidget(self.gantryIdLabel)
        self.gantryId = QtWidgets.QLineEdit(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gantryId.sizePolicy().hasHeightForWidth())
        self.gantryId.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.gantryId.setFont(font)
        self.gantryId.setStyleSheet("color: rgb(255, 193, 0);\n"
"padding-left: 5px;")
        self.gantryId.setInputMask("")
        self.gantryId.setText("")
        self.gantryId.setObjectName("gantryId")
        self.horizontalLayout_4.addWidget(self.gantryId)
        self.searchBtn = QtWidgets.QPushButton(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.searchBtn.sizePolicy().hasHeightForWidth())
        self.searchBtn.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.searchBtn.setFont(font)
        self.searchBtn.setStyleSheet("QPushButton {\n"
"  background-color: rgb(0, 0, 0);\n"
"  color: rgb(255, 193, 0);\n"
"  border: 1px;\n"
"  border-radius: 20px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 5,\n"
"                                      stop: 0 #BEBEBE, stop: 1 #323232);\n"
"}\n"
"\n"
"QPushButton:hover:!pressed{\n"
"     background-color: rgb(255, 193, 0);\n"
"     color: rgb(0, 0, 0);\n"
"\n"
"}")
        self.searchBtn.setObjectName("searchBtn")
        self.horizontalLayout_4.addWidget(self.searchBtn)
        spacerItem11 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem11)
        self.verticalLayout_3.addLayout(self.horizontalLayout_4)
        spacerItem12 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout_3.addItem(spacerItem12)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.gantryNameLabel = QtWidgets.QLabel(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gantryNameLabel.sizePolicy().hasHeightForWidth())
        self.gantryNameLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.gantryNameLabel.setFont(font)
        self.gantryNameLabel.setStyleSheet("color: rgb(255, 193, 0);\n"
"background-color: rgb(0, 0, 0);")
        self.gantryNameLabel.setScaledContents(False)
        self.gantryNameLabel.setObjectName("gantryNameLabel")
        self.horizontalLayout_2.addWidget(self.gantryNameLabel)
        self.gantryName = QtWidgets.QLineEdit(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gantryName.sizePolicy().hasHeightForWidth())
        self.gantryName.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.gantryName.setFont(font)
        self.gantryName.setPlaceholderText("")
        self.gantryName.setObjectName("gantryName")
        self.horizontalLayout_2.addWidget(self.gantryName)
        spacerItem13 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem13)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        spacerItem14 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout_3.addItem(spacerItem14)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setSpacing(6)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.gantryCostLabel = QtWidgets.QLabel(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gantryCostLabel.sizePolicy().hasHeightForWidth())
        self.gantryCostLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.gantryCostLabel.setFont(font)
        self.gantryCostLabel.setStyleSheet("color: rgb(255, 193, 0);\n"
"background-color: rgb(0, 0, 0);")
        self.gantryCostLabel.setObjectName("gantryCostLabel")
        self.horizontalLayout_3.addWidget(self.gantryCostLabel)
        self.gantryCost = QtWidgets.QLineEdit(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gantryCost.sizePolicy().hasHeightForWidth())
        self.gantryCost.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.gantryCost.setFont(font)
        self.gantryCost.setPlaceholderText("")
        self.gantryCost.setObjectName("gantryCost")
        self.horizontalLayout_3.addWidget(self.gantryCost)
        spacerItem15 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem15)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        spacerItem16 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem16)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setSpacing(6)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        spacerItem17 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem17)
        self.publishGantryBtn = QtWidgets.QPushButton(self.editPage)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.publishGantryBtn.sizePolicy().hasHeightForWidth())
        self.publishGantryBtn.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(20)
        self.publishGantryBtn.setFont(font)
        self.publishGantryBtn.setStyleSheet("QPushButton {\n"
"  background-color: rgb(0, 0, 0);\n"
"  color: rgb(255, 193, 0);\n"
"  border: 1px;\n"
"  border-radius: 20px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 5,\n"
"                                      stop: 0 #BEBEBE, stop: 1 #323232);\n"
"}\n"
"\n"
"QPushButton:hover:!pressed{\n"
"     background-color: rgb(255, 193, 0);\n"
"     color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.publishGantryBtn.setObjectName("publishGantryBtn")
        self.horizontalLayout_5.addWidget(self.publishGantryBtn)
        spacerItem18 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem18)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        self.verticalLayout_4.addLayout(self.verticalLayout_3)
        self.stackedWidget.addWidget(self.editPage)
        self.horizontalLayout_7.addWidget(self.stackedWidget)
        MainWindow.setCentralWidget(self.centralWidget)

        self.retranslateUi(MainWindow)
        self.stackedWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.gantryId, self.gantryName)
        MainWindow.setTabOrder(self.gantryName, self.gantryCost)
        MainWindow.setTabOrder(self.gantryCost, self.updateGantryBtn)
        MainWindow.setTabOrder(self.updateGantryBtn, self.registerGantryBtn)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Kapsching - Add and update gantries"))
        self.registerGantryBtn.setText(_translate("MainWindow", "Register new gantry"))
        self.updateGantryBtn.setText(_translate("MainWindow", "Update existing gantry"))
        self.deleteGantryBtn.setText(_translate("MainWindow", "Delete existing gantry"))
        self.gantryIdLabel.setText(_translate("MainWindow", "  Enter gantry ID:         "))
        self.gantryId.setPlaceholderText(_translate("MainWindow", "AA:BB:CC:11:22:33"))
        self.searchBtn.setText(_translate("MainWindow", "Search"))
        self.gantryNameLabel.setText(_translate("MainWindow", "  Enter gantry name:   "))
        self.gantryName.setStyleSheet(_translate("MainWindow", "color: rgb(255, 193, 0);\n"
"padding-left: 5px;"))
        self.gantryCostLabel.setText(_translate("MainWindow", "  Enter gantry cost:     "))
        self.gantryCost.setStyleSheet(_translate("MainWindow", "color: rgb(255, 193, 0);\n"
"padding-left: 5px;"))
        self.publishGantryBtn.setText(_translate("MainWindow", "Publish Gantry"))




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
