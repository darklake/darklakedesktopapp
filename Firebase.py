import firebase_admin
from firebase_admin import credentials, firestore

import random, string
from datetime import datetime, timedelta

import json

class Firebase:
    def __init__(self, filePathToKey='Resources/darklake_firebase_key.json'):
        self.initializeApp(filePathToKey)  
        self.referencesSetUp()

    def initializeApp(self, filePathToKey):
        if len(firebase_admin._apps) == 0:
            myCredenials = credentials.Certificate(filePathToKey)
            firebase_admin.initialize_app(myCredenials, {
                "projectId" : self.getProjectId(filePathToKey)
            })

    def getProjectId(self, filePathToKey):
        with open(filePathToKey, 'r') as key:
            return json.load(key)['project_id']

    def referencesSetUp(self):
        db = firestore.client()
        self.userReference = db.collection(u'users')
        self.macAdressReference = db.collection(u'MacAdresses')
        self.gantryPassagesReference = db.collection(u'gantryPassages')
        self.gantriesReference = db.collection(u'gantries')
            
    def readGantryInfo(self, gantryId):
        return self.gantriesReference.document(gantryId).get().to_dict()
        
    def generateRandomPassageID(self, stringLength):
        letters = (string.ascii_letters + '0123456789') * 100
        randomString = ''.join(random.choice(letters) for i in range(stringLength))
        while self.userExists(randomString):
            randomString = ''.join(random.choice(letters) for i in range(stringLength))
        return randomString
    
    def generateTimeStamp(self):
        dt = (datetime.now()-timedelta(hours=2)).strftime("%Y-%m-%d %I:%M:%S")
        return datetime.strptime(dt, "%Y-%m-%d %I:%M:%S")
        
    def accumulateCharge(self, userID, cost):
        accumulatedCharges = self.userReference.document(userID).get().to_dict()['accumulated_charges'] + cost
        self.userReference.document(userID).set({
            u'accumulated_charges' : accumulatedCharges
            }, merge=True)
        
    def addPassage(self, userID, cost, gantryId):
        self.accumulateCharge(userID, cost)
        self.userReference.document(userID).collection(u'passages').document(self.generateRandomPassageID(20)).set({
            u'cost' : cost,
            u'gId' : gantryId,
            u'time' : self.generateTimeStamp()
            })
        
    def userExists(self, userId):
        return self.userReference.document(userId).get().to_dict() is not None
       
    def addGantry(self, gantryId, cost, name, position):
        self.gantriesReference.document(gantryId).set({
            u'cost' : cost,
            u'name' : name,
            u'position' : firestore.GeoPoint(position[0], position[1])
            }, merge=True)

        self.gantryPassagesReference.document(gantryId).set({
            u'counter' : 0,
            u'uId' : 'None'
        })
        
    def updateGantryWithoutPosition(self, gantryId, cost, name):
        self.gantriesReference.document(gantryId).set({
            u'cost' : cost,
            u'name' : name
            }, merge=True)

    def addGantryToPassageList(self, gantryId):
        self.gantryPassagesReference.document(gantryId).set({
            u'uId' : "None",
        })
        
    def updateGantryPosition(self, gantryId, position):
        self.gantriesReference.document(gantryId).set({
            u'position' : firestore.GeoPoint(position[0], position[1])
            }, merge=True)

    def deleteGantry(self, gantryId):
        self.gantriesReference.document(gantryId).delete()
        self.gantryPassagesReference.document(gantryId).delete()
            
    def subscribe(self, bluetoothMacAddress, function):
        self.gantryPassagesReference.document(bluetoothMacAddress).on_snapshot(function)

if __name__ == '__main__':
    firebase = Firebase()
